import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BodydetailsComponent } from './bodydetails/bodydetails.component';
import { ChiSiamoComponent } from './chi-siamo/chi-siamo.component';
import { DestroyComponent } from './destroy/destroy.component';
import { HomeIndexComponent } from './home-index/home-index.component';
import { LoginComponent } from './login/login.component';
import { ModificauserComponent } from './modificauser/modificauser.component';
import { NewuserComponent } from './newuser/newuser.component';
import { SbloccarotteService } from './services/sbloccarotte.service';
import { TableSystemComponent } from './table-system/table-system.component';
import { TableuserComponent } from './tableuser/tableuser.component';

const routes: Routes = [
  { path: '', redirectTo:'home', pathMatch: 'full' },  
  { path: 'home', component:HomeIndexComponent},
  { path: 'Sistemasolare', component:TableSystemComponent},
  { path: 'ciao', component:ChiSiamoComponent},
  { path: 'Sistemasolare/:id/dettagli', component:BodydetailsComponent},
  { path: 'Sistemasolare/:id/destroy', component:DestroyComponent},
  { path: 'login', component:LoginComponent},
  { path: 'users/:id/modifica', component:ModificauserComponent},
  { path: 'users', component:TableuserComponent, canActivate:[SbloccarotteService]},
  { path: 'newuser', component:NewuserComponent},
  
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
