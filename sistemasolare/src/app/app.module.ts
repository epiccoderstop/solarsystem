import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeIndexComponent } from './home-index/home-index.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { TableSystemComponent } from './table-system/table-system.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChiSiamoComponent } from './chi-siamo/chi-siamo.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { BodydetailsComponent } from './bodydetails/bodydetails.component';
import { DestroyComponent } from './destroy/destroy.component';
import { NavbaruserComponent } from './navbaruser/navbaruser.component';
import { LoginComponent } from './login/login.component';
import { TableuserComponent } from './tableuser/tableuser.component';
import { ModificauserComponent } from './modificauser/modificauser.component';
import { NewuserComponent } from './newuser/newuser.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeIndexComponent,
    NavbarComponent,
    FooterComponent,
    TableSystemComponent,
    ChiSiamoComponent,
    BodydetailsComponent,
    DestroyComponent,
    NavbaruserComponent,
    LoginComponent,
    TableuserComponent,
    ModificauserComponent,
    NewuserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
