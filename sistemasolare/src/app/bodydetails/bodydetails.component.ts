import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-bodydetails',
  templateUrl: './bodydetails.component.html',
  styleUrls: ['./bodydetails.component.css']
})
export class BodydetailsComponent implements OnInit {

  dettagliCorpi!:any

  constructor( private mainservice: MainService, private route:ActivatedRoute) { }

  ngOnInit(): void {
  this.route.params.subscribe(params =>this.mainservice.getbodybyid(params.id).subscribe(resp=> this.dettagliCorpi=resp));
  }

}
