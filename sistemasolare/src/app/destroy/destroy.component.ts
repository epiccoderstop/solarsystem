import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-destroy',
  templateUrl: './destroy.component.html',
  styleUrls: ['./destroy.component.css']
})
export class DestroyComponent implements OnInit {

  corpoDistrutto!:any;

  constructor(private service:MainService, private route:ActivatedRoute) { }

  ngOnInit(): void {
  this.route.params.subscribe(params =>this.service.getbodybyid(params.id).subscribe(data => this.corpoDistrutto = data))
  }

}
