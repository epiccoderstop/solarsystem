export interface Ibodies {
  id: string;
  name: string;
  englishName: string;
  isPlanet: boolean;
  moons?: any;
  semimajorAxis: number;
  perihelion: number;
  aphelion: number;
  eccentricity: number;
  inclination: number;
  mass: Mass;
  vol: Vol;
  density: number;
  gravity: number;
  escape: number;
  meanRadius: number;
  equaRadius: number;
  polarRadius: number;
  flattening: number;
  dimension: string;
  sideralOrbit: number;
  sideralRotation: number;
  aroundPlanet: AroundPlanet;
  discoveredBy: string;
  discoveryDate: string;
  alternativeName: string;
  axialTilt: number;
  avgTemp: number;
  mainAnomaly: number;
  argPeriapsis: number;
  longAscNode: number;
  rel: string;
}

interface AroundPlanet {
  planet: string;
  rel: string;
}

interface Vol {
  volValue: number;
  volExponent: number;
}

interface Mass {
  massValue: number;
  massExponent: number;
}

