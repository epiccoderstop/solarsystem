import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SbloccarotteService } from '../services/sbloccarotte.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email ='';
  password ='';

  constructor(private service: SbloccarotteService, private router: Router) { }

  ngOnInit(): void {
  }
logmein(){
  this.service.loginapp(this.email, this.password);
  this.router.navigate(['/users']);
}


}
