import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-modificauser',
  templateUrl: './modificauser.component.html',
  styleUrls: ['./modificauser.component.css']
})
export class ModificauserComponent implements OnInit {
  user: any = { name: '', username: '', email: '', city: '' };

  constructor(private UsersService: UsersService, private Rotte: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.Rotte.params.subscribe(params => this.UsersService.getuser(params.id).subscribe(data2 => this.user = data2));
  }

  confirmupdate(): void {
  this.UsersService.updateuser(this.user).subscribe(resp => {alert('oggetto modificato'+JSON.stringify(resp))
  })
  this.router.navigate(['users'])
  }
}
