import { UsersService } from './../services/users.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.css']
})
export class NewuserComponent implements OnInit {
  nuovouser: any = { name: '', username: '', email: '', address: { city: '' } };

  constructor(private service: UsersService, private router: Router) { }

  ngOnInit(): void {
  }
  newuser() {
    this.service.postuser(this.nuovouser).subscribe(resp => { alert('utente aggiunto' + JSON.stringify(resp));
   });
    this.router.navigate(['users'])
  }
}
