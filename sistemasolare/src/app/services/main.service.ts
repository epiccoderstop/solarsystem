import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MainService {
  private urlAPI = 'https://api.le-systeme-solaire.net/rest/'

  constructor(private http: HttpClient) { 
};

getbodies(){
return this.http.get<any>(this.urlAPI +'bodies')
}

getbodybyid(takafumi:string){
return this.http.get<any>(this.urlAPI +'bodies/'+takafumi)
}

deletebody(luigi:string){
return this.http.delete(this.urlAPI +'bodies/'+luigi)

}


}
