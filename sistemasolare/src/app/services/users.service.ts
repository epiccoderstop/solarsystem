import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private api2 = 'https://jsonplaceholder.typicode.com/users/';

  constructor(private http: HttpClient) { }

  getusers(){
    return this.http.get<any>(this.api2)
  }

  deleteuser(id:number){
    return this.http.delete(this.api2+id)
    
  }

  getuser(id:number){
    return this.http.get(this.api2+id)
  }

  updateuser(user:any){
    return this.http.put(this.api2+user.id, user)
  }

  postuser(user:any){
    return this.http.post(this.api2, user)
  }
}

