import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from '../services/main.service';

@Component({
  selector: 'app-table-system',
  templateUrl: './table-system.component.html',
  styleUrls: ['./table-system.component.css']
})
export class TableSystemComponent implements OnInit {
 universal!:any;

  constructor(private service:MainService, private router: Router) { }

  ngOnInit(): void {
  this.service.getbodies().subscribe (response => this.universal = response);
  }

  infobody(ernesto:any){
  this.router.navigate(['Sistemasolare',ernesto.id,'dettagli'])
  }

  removebody (item:any){
  alert('Stai distruggendo un Corpo celeste ')
  /* this.service.deletebody(item.id).subscribe(response => this.service.getbodies().subscribe(response=> this.universal = response); */
  this.router.navigate(['Sistemasolare',item.id,'destroy'])


  }

}
