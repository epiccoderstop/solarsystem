import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../services/users.service';


@Component({
  selector: 'app-tableuser',
  templateUrl: './tableuser.component.html',
  styleUrls: ['./tableuser.component.css']
})
export class TableuserComponent implements OnInit {

  users!: any

  constructor(private service: UsersService, private router: Router, private Rotte: ActivatedRoute) { }

  ngOnInit(): void {
    this.service.getusers().subscribe(response => this.users = response);
  }

  remove(dati: any) {
    alert('User removed')
    /* this.service.deleteuser(dati.id).subscribe(resp => this.service.getusers().subscribe(data => this.users = data)); */
    this.users.splice(this.users.indexOf(dati), 1)
  }

  update(dati: any){
this.router.navigate(['users',dati.id,'modifica']) 
  }
}
